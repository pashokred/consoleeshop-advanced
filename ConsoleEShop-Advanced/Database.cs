﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Advanced
{
    static class Database
    {
        private static List<Product> availableProducts;
        private static List<Order> orders;
        private static List<User> users;

        public static void AddProduct(Product product)
        {
            availableProducts ??= new List<Product>();
            if(!availableProducts.Exists(p => p == product) && product != null)
                availableProducts.Add(product);
        }

        public static void AddOrder(Order order)
        {
            orders ??= new List<Order>();
            if (!orders.Exists(o => o.OrderId == order.OrderId) && order != null)
                orders.Add(order);
        }

        public static void AddUser(User user)
        {
            users ??= new List<User>();
            if (!users.Exists(u => u == user) && user != null)
                users.Add(user);
        }

        public static Order GetOrderById(int id)
        {
            return orders.Find(o => o.OrderId == id);
        }

        public static Product GetProductByName(string name)
        {
            return availableProducts?.Find(x => x.Name == name);
        }

        public static void PrintProducts()
        {
            Console.WriteLine("Product list:\n\n");
            foreach (Product product in availableProducts)
            {
                Console.WriteLine($"Name: {product.Name} \n" +
                    $"Description: {product.Description}\n" +
                    $"Price: {product.Price}\n" +
                    $"Category: {product.Category}");
            }
        }
        public static void ChangeOrderStatus(int orderId, OrderStatus status)
        {
            if(GetOrderById(orderId) == null)
            {
                Console.WriteLine($"There is no order with order id {orderId}");
                return;
            }
            var index = orders.FindIndex(o => o.OrderId == orderId);
            orders[index].Status = status;
        }
    }
}
