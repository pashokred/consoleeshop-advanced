﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Advanced
{
    class Order
    {
        public Order(List<Product> products, DateTime date, OrderStatus status, int orderId)
        {
            Products = products;
            Date = date;
            Status = status;
            OrderId = orderId;
        }

        public int OrderId { get; set; }
        public List<Product> Products { get; set; }
        public DateTime Date { get; set; }
        public OrderStatus Status { get; set; }
    }

    enum OrderStatus
    {
        New,
        CancelledByAdmin,
        CancelledByUser,
        PaymentSuccessful,
        Sent,
        Received
    }
}
