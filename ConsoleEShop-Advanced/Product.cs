﻿namespace ConsoleEShop_Advanced
{
    class Product
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public Category Category { get; set; }

        public Product(string name, string description, int price, Category category)
        {
            Name = name;
            Description = description;
            Price = price;
            Category = category;
        }
    }

    enum Category
    {
        Technology,
        Tools,
        SportStock,
        Clothes,
        Health,
        Food
    }
}
