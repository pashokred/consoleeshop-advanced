﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Advanced
{
    class Visitor
    {
        public void PrintProductList()
        {
            Database.PrintProducts();
        }

        public Product GetProductByName(string name)
        {
            return Database.GetProductByName(name);
        }
    }
}
