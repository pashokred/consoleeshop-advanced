﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Advanced
{
    class User : Visitor
    {
        public List<Order> Orders { get; set; }
        
        public string FullName { get; private set; }
        public string Email { get; private set; }
        public string PhoneNumber { get; private set; }
        public string Password { get; private set; }

        public User(string fullName, string email, string phoneNumber, string password)
        {
            FullName = fullName;
            Email = email;
            PhoneNumber = phoneNumber;
            Password = password;
        }

        public int NewOrder(List<Product> products)
        {
            int orderId = -1;
            Orders ??= new List<Order>();
            if(products != null)
            {
                orderId = Orders.Count;
                Orders.Add(new Order(products, DateTime.Now, OrderStatus.New, orderId));
            }
            return orderId;
        }

        public void Checkout(int orderId)
        {
            Order order = Orders.Find(o => o.OrderId == orderId);
            if (order == null)
            {
                Console.WriteLine($"There are no orders with id {orderId}. To checkout you need to make a new order firstly\n" +
                    "Do you want to make a new order? If YES enter 1, if NO enter 0\n");
                string MakeNewOrder = Console.ReadLine();
                if (Convert.ToBoolean(MakeNewOrder)) ;
                return;
            }
            if(order.Status != OrderStatus.New)
            {
                Console.WriteLine($"Order with id {orderId} is has status {order.Status}. To checkout it must be new\n");
                return;
            }
            if(Database.GetOrderById(orderId) == null)
            {
                Console.WriteLine($"Order with id {orderId} has already checked out\n");
                return;
            }
            Database.AddOrder(order);
        }

        public void Cancel(int orderId)
        {
            Order order = Database.GetOrderById(orderId);
            if (order == null)
            {
                Console.WriteLine($"There are no orders with id {orderId}\n");
                return;
            }
            order.Status = OrderStatus.CancelledByUser;
        }

        public void OrdersHistory()
        {
            foreach(Order order in Orders)
            {
                Console.WriteLine($"Orders history:\n\n" +
                    $"Id: {order.OrderId}, status: {order.Status}, date: {order.Date}, products amount: {order.Products.Count}\n");
            }
        }

        public void ReceiveOrder(int orderId)
        {
            var order = Database.GetOrderById(orderId);
            if(order == null || order.Status != OrderStatus.Sent)
            {
                Console.WriteLine($"There are no sent orders with id {orderId}\n" +
                    $"Please, make sure you have checked out order with id {orderId} or connect with admin");
                return;
            }
            Database.ChangeOrderStatus(orderId, OrderStatus.Received);
        }

        public void ChangePersonalInfo()
        {
            Start:
            Console.WriteLine("What do you want to change?\n" +
                "1 - Full Name\n" +
                "2 - Email\n" +
                "3 - Phone number\n" +
                "4 - Password\n" +
                "5 - Exit\n");
            string operation = Console.ReadLine();
            switch (Convert.ToInt16(operation))
            {
                case 1:
                    Console.WriteLine("Enter new full name: ");
                    FullName = Console.ReadLine();
                    break;
                case 2:
                    Console.WriteLine("Enter new email: ");
                    Email = Console.ReadLine();
                    break;
                case 3:
                    Console.WriteLine("Enter new phone number: ");
                    PhoneNumber = Console.ReadLine();
                    break;
                case 4:
                    Console.WriteLine("Enter new password: ");
                    Password = Console.ReadLine();
                    break;
                case 5:
                    goto Exit;
                default:
                    Console.WriteLine("Invalid command number, try again:\n");
                    goto Start;
            }
            Console.WriteLine("\nUpdated successfully!\n");
            Exit:;
        }

        public void SignOut()
        {
            throw new NotImplementedException();
        }
    }
}
